import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Dadard Website';
  subtitle = 'Eddard Stark memorial';
  urlEddardPortrait = 'assets/images/eddard_stark_portrait.png';
}
